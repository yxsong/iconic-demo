<?php
/**
 * Created by PhpStorm.
 * User: Ujjwol
 * Date: 12/16/2019
 * Time: 6:46 PM
 */

namespace App\Services\Demo;


//use App\DTO\ProductDTO;
use Illuminate\Http\Request;

interface IProductService
{
    public function getProducts();
}
