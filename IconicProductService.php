<?php
/**
 * Created by PhpStorm.
 * User: Ujjwol
 * Date: 12/16/2020
 * Time: 6:48 PM
 */

namespace App\Services\Demo;

//use App\DTO\ProductDTO;
use GuzzleHttp\Client;

class IconicProductService extends HttpService implements IProductService, IVideoService, IDecorateService
{
    const HEADERS = [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
    ];
    const BASE_URL = 'https://eve.theiconic.com.au/';

    protected $products;
    protected $decoratedProducts;

    public function __construct()
    {
        parent::__construct();
    }

    public function setProducts($products)
    {
        $this->products = $products;
    }

    public function getProducts()
    {
        $jsonProducts = $this->getData('GET', 'catalog/products', ['query' => [
            'gender' => 'female',
            'page' => 1,
            'page_size' => 10,
            'sort' => 'popularity',
        ]]);

        $this->setProducts($jsonProducts);

        return $this->products;
    }

    /**
     * Get videos for products
     * @param $sku
     * @return array
     */
    public function getPreviewVideos($sku)
    {
        $videos = $this->getData('GET', "catalog/products/{$sku}/videos");

        return $videos['_embedded']['videos_url'];
    }

    /**
     * Merge videos (if any) with products
     */
    public function decorateProducts()
    {
        $decoratedProducts = [];

        foreach ($this->products as $product)
        {
            // No videos
            if ((int)$product['video_count'] === 0)
            {
                continue;
            }

            $videos = $this->getPreviewVideos($product['sku']); // May do json decode

            if (!empty($videos))
            {
                $decoratedProducts[] = array_merge($product, ['videos_previews' => $videos]);
            }
        }

        $this->decoratedProducts = $decoratedProducts;
    }

    /**
     * Get decorated products
     */
    public function getDecoratedProducts()
    {
        return $this->decoratedProducts;
    }
}
