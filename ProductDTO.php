<?php

namespace App\DTO;

class ProductDTO
{
    private $video_count;
    private $price;
    private $markdown_price;
    private $special_price;
    private $returnable;
    private $final_sale;
    private $stock_update;
    private $final_price;
    private $sku;
    private $name;
    private $ribbon;
    private $messaging;
    private $color_name_brand;
    private $short_description;
    private $shipment_type;
    private $color_name;
    private $color_hex;
    private $cart_price_rules;
    private $attributes;
    private $simples;
    private $sustainability;
    private $link;
    private $activated_at;
    private $return_policy_message;
    private $categories_translated;
    private $category_path;
    private $category_ids;
    private $related_products;
    private $image_products;
    private $attribute_set_identifier;
    private $supplier;
    private $wannaby_id;
    private $citrus_ad_id;
    private $associated_skus;
    private $size_guide_url;
    private $related;
    private $variants;
    private $campaign_details;
    private $_embedded;
    private $_links;

    public function __construct($data)
    {
        $this->video_count = $data ['video_count'];
        $this->price = $data ['price'];
        $this->markdown_price = $data ['markdown_price'];
        $this->special_price = $data ['special_price'];
        $this->returnable = $data ['returnable'];
        $this->final_price = $data ['final_price'];
        $this->final_sale = $data ['final_sale'];
        $this->stock_update = $data ['stock_update'];
        $this->final_price = $data ['final_price'];
        $this->sku = $data ['sku'];
        $this->name = $data ['name'];
        $this->ribbon = $data ['ribbon'];
        $this->messaging = $data ['messaging'];
        $this->color_name_brand = $data ['color_name_brand'];
        $this->short_description = $data ['short_description'];
        $this->shipment_type = $data ['shipment_type'];
        $this->color_name = $data ['color_name'];
        $this->color_hex = $data ['color_hex'];
        $this->cart_price_rules = $data ['cart_price_rules'];
        $this->attributes = $data ['attributes'];
        $this->simples = $data ['simples'];
        $this->sustainability = $data ['sustainability'];
        $this->link = $data ['link'];
        $this->activated_at = $data ['activated_at'];
        $this->return_policy_message = $data ['return_policy_message'];
        $this->categories_translated = $data ['categories_translated'];
        $this->category_path = $data ['category_path'];
        $this->category_ids = $data ['category_ids'];
        $this->related_products = $data ['related_products'];
        $this->image_products = $data ['image_products'];
        $this->attribute_set_identifier = $data ['attribute_set_identifier'];
        $this->supplier = $data ['supplier'];
        $this->wannaby_id = $data ['wannaby_id'];
        $this->citrus_ad_id = $data ['citrus_ad_id'];
        $this->associated_skus = $data ['associated_skus'];
        $this->size_guide_url = $data ['size_guide_url'];
        $this->related = $data ['related'];
        $this->variants = $data ['variants'];
        $this->campaign_details = $data ['campaign_details'];
        $this->_embedded = $data ['_embedded'];
        $this->_links = $data ['_links'];
    }

    /**
     * @return mixed
     */
    public function getVideoCount()
    {
        return $this->video_count;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getMarkdownPrice()
    {
        return $this->markdown_price;
    }

    /**
     * @return mixed
     */
    public function getSpecialPrice()
    {
        return $this->special_price;
    }

    /**
     * @return mixed
     */
    public function getReturnable()
    {
        return $this->returnable;
    }

    /**
     * @return mixed
     */
    public function getFinalSale()
    {
        return $this->final_sale;
    }

    /**
     * @return mixed
     */
    public function getStockUpdate()
    {
        return $this->stock_update;
    }

    /**
     * @return mixed
     */
    public function getFinalPrice()
    {
        return $this->final_price;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getRibbon()
    {
        return $this->ribbon;
    }

    /**
     * @return mixed
     */
    public function getMessaging()
    {
        return $this->messaging;
    }

    /**
     * @return mixed
     */
    public function getColorNameBrand()
    {
        return $this->color_name_brand;
    }

    /**
     * @return mixed
     */
    public function getShortDescription()
    {
        return $this->short_description;
    }

    /**
     * @return mixed
     */
    public function getShipmentType()
    {
        return $this->shipment_type;
    }

    /**
     * @return mixed
     */
    public function getColorName()
    {
        return $this->color_name;
    }

    /**
     * @return mixed
     */
    public function getColorHex()
    {
        return $this->color_hex;
    }

    /**
     * @return mixed
     */
    public function getCartPriceRules()
    {
        return $this->cart_price_rules;
    }

    /**
     * @return mixed
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @return mixed
     */
    public function getSimples()
    {
        return $this->simples;
    }

    /**
     * @return mixed
     */
    public function getSustainability()
    {
        return $this->sustainability;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return mixed
     */
    public function getActivatedAt()
    {
        return $this->activated_at;
    }

    /**
     * @return mixed
     */
    public function getReturnPolicyMessage()
    {
        return $this->return_policy_message;
    }

    /**
     * @return mixed
     */
    public function getCategoriesTranslated()
    {
        return $this->categories_translated;
    }

    /**
     * @return mixed
     */
    public function getCategoryPath()
    {
        return $this->category_path;
    }

    /**
     * @return mixed
     */
    public function getCategoryIds()
    {
        return $this->category_ids;
    }

    /**
     * @return mixed
     */
    public function getRelatedProducts()
    {
        return $this->related_products;
    }

    /**
     * @return mixed
     */
    public function getImageProducts()
    {
        return $this->image_products;
    }

    /**
     * @return mixed
     */
    public function getAttributeSetIdentifier()
    {
        return $this->attribute_set_identifier;
    }

    /**
     * @return mixed
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @return mixed
     */
    public function getWannabyId()
    {
        return $this->wannaby_id;
    }

    /**
     * @return mixed
     */
    public function getCitrusAdId()
    {
        return $this->citrus_ad_id;
    }

    /**
     * @return mixed
     */
    public function getAssociatedSkus()
    {
        return $this->associated_skus;
    }

    /**
     * @return mixed
     */
    public function getSizeGuideUrl()
    {
        return $this->size_guide_url;
    }

    /**
     * @return mixed
     */
    public function getRelated()
    {
        return $this->related;
    }

    /**
     * @return mixed
     */
    public function getVariants()
    {
        return $this->variants;
    }

    /**
     * @return mixed
     */
    public function getCampaignDetails()
    {
        return $this->campaign_details;
    }

    /**
     * @return mixed
     */
    public function getEmbedded()
    {
        return $this->_embedded;
    }

    /**
     * @return mixed
     */
    public function getLinks()
    {
        return $this->_links;
    }

    public function toArray()
    {
        return [
            'video_count' => $this->video_count,
            'price' => $this->price,
            'markdown_price' => $this->markdown_price,
            'special_price' => $this->special_price,
            'returnable' => $this->returnable,
            'final_sale' => $this->final_price,
            'stock_update' => $this->stock_update,
            'final_price' => $this->final_price,
            'sku' => $this->sku,
            'name' => $this->name,
            'ribbon' => 'new',
            'messaging' => $this->messaging,
            'color_name_brand' => $this->color_name_brand,
            'short_description' => $this->short_description,
            'shipment_type' => $this->shipment_type,
            'color_name' => $this->color_name,
            'color_hex' => $this->color_hex,
            'cart_price_rules' => $this->cart_price_rules,
            'attributes' => $this->attributes,
            'simples' => $this->simples,
            'sustainability' => $this->sustainability,
            'link' => $this->link,
            'activated_at' => $this->activated_at,
            'return_policy_message' => $this->return_policy_message,
            'categories_translated' => $this->categories_translated,
            'category_path' => $this->category_path,
            'category_ids' => $this->category_ids,
            'related_products' => $this->related_products,
            'image_products' => $this->image_products,
            'attribute_set_identifier' => $this->attribute_set_identifier,
            'supplier' => $this->supplier,
            'wannaby_id' => $this->wannaby_id,
            'citrus_ad_id' => $this->citrus_ad_id,
            'associated_skus' => $this->associated_skus,
            'size_guide_url' => $this->size_guide_url,
            'related' => $this->related,
            'variants' => $this->variants,
            'campaign_details' => $this->campaign_details,
            '_embedded' => $this->_embedded,
            '_links' => $this->_links
        ];
    }
}
