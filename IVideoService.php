<?php
namespace App\Services\Demo;

interface IVideoService
{
    public function getPreviewVideos($sku);
}