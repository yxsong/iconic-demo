<?php

namespace App\Console\Commands;

use App\Services\Demo\IconicProductService;
use App\Services\Demo\IProductService;
use App\Services\Demo\Outputer;
use Illuminate\Console\Command;

class BootstrapIconic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:bootstrapIconic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param IProductService $productService => new IconicProductService()
     * @return int
     */
    public function handle(IProductService $productService)
    {
        // Get products
        $products = $productService->getProducts();

        if (!empty($products))
        {
            // Get videos (if any) and decorate products accordingly
            $productService->decorateProducts();

            $outputer = new Outputer($productService->getDecoratedProducts());
            $outputer->toJSON();

            echo 1;
        }
        else
        {
            echo 0;
        }

        return 0;
    }
}
