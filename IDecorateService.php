<?php
namespace App\Services\Demo;

interface IDecorateService
{
    public function decorateProducts();

    public function getDecoratedProducts();
}