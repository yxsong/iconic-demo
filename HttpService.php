<?php
namespace App\Services\Demo;

use GuzzleHttp\Client;

abstract class HttpService
{
    const HEADERS = [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
    ];
    const BASE_URL = 'https://eve.theiconic.com.au/';

    protected $client;

    public function __construct()
    {
        $this->client = new Client(['headers' => self::HEADERS]);
    }

    public function getData($method, $path, array $queryParams = [])
    {
        $request = $this->client->request($method, self::BASE_URL . $path, $queryParams);

        return json_decode($request->getBody()->getContents(), true);
    }
}