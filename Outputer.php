<?php
namespace App\Services\Demo;

class Outputer
{
    protected $data;

    public function __construct($data = [])
    {
        $this->data = $data;
    }

    public function toJSON()
    {
        return $this->data->toJSON();
    }
}
